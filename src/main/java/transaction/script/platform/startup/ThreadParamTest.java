package transaction.script.platform.startup;

import java.util.concurrent.TimeUnit;

class ThreadParam implements Runnable {
	static int c;

	public ThreadParam(int a, int b) {
		c = a + b;
		System.out.println("asdsad");
	}

	public void run() {
		System.out.println("Name of thred is ::" + Thread.currentThread().getName() + "\tand value of c is ::" + c);
	}
	//http://stackoverflow.com/questions/12049369/how-to-pass-parameter-to-an-already-running-thread-in-java

}

public class ThreadParamTest {
	public static void main(String args[]) throws InterruptedException {
		Runnable r = new ThreadParam(1000, 2000);
		new Thread(r).start();
		Runnable r2 = new ThreadParam(2000, 2000);
		new Thread(r2).start();
		Runnable r3 = new ThreadParam(3000, 2000);
		new Thread(r3).start();
		Runnable r4 = new ThreadParam(4000, 2000);
		new Thread(r4).start();
		Runnable r5 = new ThreadParam(5000, 2000);
		new Thread(r5).start();
		Runnable r6 = new ThreadParam(6000, 2000);
		new Thread(r6).start();
		Runnable r7 = new ThreadParam(7000, 2000);
		new Thread(r7).start();
	}
}