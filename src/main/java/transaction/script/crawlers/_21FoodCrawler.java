package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class _21FoodCrawler {
	public static final Logger LOGGER = LoggerFactory.getLogger(_21FoodCrawler.class);

	public static void main(String[] args) {
		String baseUrl = "http://www.21food.com";
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0;
		for (int i = 1; i <= 13; i++) {
			try {
				//Document document = Jsoup.parse(IOUtils.toString(new URL("http://www.21food.com/company/search_keys-dairy+product_c-Ukraine-p"+i+".html")));
				Document document = null;
				try {
					document = Jsoup.connect(new URL("http://www.21food.com/company/search_keys-dairy+product_c-Ukraine-p" + i + ".html").toString())
							.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(10000).get();
				} catch (SocketTimeoutException | HttpStatusException e) {
					LOGGER.error("connection timed out...");
				}
				TimeUnit.MILLISECONDS.sleep(2500);
				int index = 0;
				while (document != null && true) {
					String businessRange = "", products = "", address = "", mobile = "", otherMobile = "", bizName = "", website = "";
					Row row = sheet.createRow(rowNumber);
					try {
						String text = document.getElementsByClass("link_t1").get(index).attr("href");
						Document linkDocument = null;
						try {
							if (text.endsWith("21food.com")) {
								index++;
								continue;
							} else {
								for (int j = 0; j < 3; j++) {
									try {
										linkDocument = Jsoup.connect(new URL(baseUrl + text.replace("\"", "%22")).toString())
												.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
												.timeout(60000).get();
										break;
									} catch (SocketTimeoutException | SocketException e) {
										e.printStackTrace();
										if (j < 3)
											LOGGER.info("timed out....trying again");
										else
											break;
									}
								}
							}
						} catch (HttpStatusException e) {
							index++;
							e.printStackTrace();
							continue;
						}
						TimeUnit.MILLISECONDS.sleep(500);
						bizName = linkDocument.getElementsByClass("webname").text();
						Elements rows = linkDocument.getElementsByClass("marg_x15");
						for (Element element : rows) {
							if (element.toString().contains("Basic Information</span>")) {
								int innerIndex = 0;
								while (true) {
									try {
										Element subElement = element.getElementsByTag("tr").get(innerIndex);
										if (subElement.getElementsByTag("td").get(0).text().toString().contains("Business Range:")) {
											businessRange = subElement.getElementsByTag("td").get(0).nextElementSibling().text().trim();
										} else if (subElement.getElementsByTag("td").get(0).text().toString().contains("Main Products:")) {
											products = subElement.getElementsByTag("td").get(0).nextElementSibling().text().trim();
										}
										innerIndex++;
									} catch (IndexOutOfBoundsException e) {
										//e.printStackTrace();
										break;
									}
								}
							}
						}
						Elements elements = linkDocument.getElementsByAttributeValue("bgcolor", "#F3F3F3").get(0).getElementsByTag("td");
						for (int j = 0; j < elements.size(); j++) {
							if (elements.get(j).text().toString().contains("Address")) {
								j += 1;
								address = elements.get(j).text();
							} else if (elements.get(j).text().toString().contains("Tel")) {
								j += 1;
								mobile = elements.get(j).text();
							} else if (elements.get(j).text().toString().contains("Fax")) {
								j += 1;
								otherMobile = elements.get(j).text();
							} else if (elements.get(j).text().toString().contains("Website")) {
								j += 1;
								website = elements.get(j).toString().split("'")[1];
							}
						}
						index++;
						row.createCell(0).setCellValue(bizName);
						row.createCell(1).setCellValue(mobile);
						if (products.isEmpty())
							row.createCell(2).setCellValue(businessRange);
						else
							row.createCell(2).setCellValue(products);
						row.createCell(3).setCellValue(address);
						row.createCell(4).setCellValue(website);
						row.createCell(5).setCellValue(otherMobile);
						rowNumber++;
						System.out.println("Page number: " + i + "row number: " + index);
					} catch (IndexOutOfBoundsException e) {
						break;
					}
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (workbook != null) {
			try (FileOutputStream outputStream = new FileOutputStream(new File("/home/abhi/Desktop/sheets/test.xlsx"));) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		LOGGER.info("Entered details into sheet. Have a great day...\u263A \u263A");
	}
}